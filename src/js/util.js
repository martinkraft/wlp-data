/**
 * JS Utility Class
 * @author Martin Kraft mail@martinkraft.com
 */

(function ($) {
    var MJK = window.MJK = window.MJK || {};

    MJK.$win = $(window);
    MJK.$doc = $(document);
    MJK.$body = $('body');
    MJK.$html = $('html').addClass('js');
    MJK.$main = $('main');

    MJK.isMobile = false;
    MJK.hasTouch = false;
    function checkTouch(){
        MJK.hasTouch = true;
        MJK.$doc.off('touchstart', checkTouch);
    }
    MJK.$doc.on('touchstart', checkTouch);

    MJK.lang = MJK.$html.attr('lang') || 'de';
    MJK.startTime = new Date().getTime();
    MJK.isLocal = window.location.hostname === 'localhost';
    MJK.debug = true; //$('meta[name="environment"]').attr('content') !== 'live';

    MJK.locale = $('meta[name="language"]').attr('content') || 'de-DE';

    MJK.getTimeStamp = function () {
        return new Date().getTime() - MJK.startTime;
    };

    MJK.log = (function () {
        if (!MJK.debug) return function () {
        };
        var visibleLog = false;

        var log = console ? function () {
            var a = Array.prototype.slice.call(arguments);
            a.unshift('MJK ');
            console.log.apply(console, a);

            a.shift();

            if (visibleLog) {
                var str = '', e, eStr;
                while (e = a.shift()) {
                    eStr = '' + e;

                    if (typeof e == 'object' || typeof e == 'array') {
                        try {
                            eStr = JSON.stringify(e, null, 2)
                        } catch (e) {

                        }
                    }

                    str += eStr;
                }

                $('body').append('<p><code><pre>MJK ' + MJK.getTimeStamp() + ': ' + str + '</pre></code></p>');
            }
        } : function () {
        };
        return log;
    })();

    var util = MJK.util = {

        path: (function () {
            if (!window.hasOwnProperty('URLSearchParams')) {
                function warning() {
                    MJK.log('URLSearchParams is not defined');
                }

                warning();
                return {
                    getPathAndParams: function () {
                        warning();
                        return {};
                    },
                    setParams: function (url) {
                        warning();
                        return url;
                    },
                    appendParams: function (url) {
                        warning();
                        return url;
                    },
                    setLocationParams: function () {
                        warning();
                        return window.location.href;
                    },
                    appendLocationParams: function () {
                        warning();
                        return window.location.href;
                    }
                };
            } else {
                function getPathAndParams(url) {
                    var baseUrl = url,
                        hi = url.indexOf('#'),
                        qi = url.indexOf('?');

                    if (hi !== -1 && hi > qi) {
                        var hash = url.substr(hi + 1);
                        url = url.substr(0, hi);
                    }

                    return {
                        url: url,
                        hash: hash,
                        path: (qi == -1) ? url : url.substr(0, qi),
                        params: new URLSearchParams((qi == -1) ? '' : url.substr(qi + 1))
                    }
                }

                function setParams(url, params, append) {
                    var path = getPathAndParams(url);

                    for (var key in params) if (params.hasOwnProperty(key)) {
                        path.params[append ? 'append' : 'set'](key, params[key]);
                    }

                    url = path.path + '?' + path.params.toString();
                    if (path.hash) url += '#' + path.hash;

                    return url;
                }

                function setLocationParams(params, append) {
                    var url = setParams(window.location.href, params, append);
                    window.history.pushState({},
                        document.title,
                        url
                    );
                    return url;
                }

                return {
                    getPathAndParams: getPathAndParams,
                    setParams: setParams,
                    appendParams: function (url, params) {
                        return setParams(url, params, true);
                    },
                    setLocationParams: setLocationParams,
                    appendLocationParams: function (url, params) {
                        return setLocationParams(params, true);
                    },
                }
            }
        })(),

        processDynamic: (function () {
            var callbacks = [];
            processFunc = function ($context) {
                for (var i = 0; i < callbacks.length; i++) {
                    callbacks[i]($context);
                }
            };

            processFunc.add = function (callback, noDirectExecution) {
                if (typeof callback != 'function') {
                    MJK.log(callback, ' ist keine Funktion');
                    return;
                }

                callbacks.push(callback);

                if (!noDirectExecution) callback(MJK.$doc);
            };

            processFunc.remove = function (callback) {
                var i = callbacks.length;
                while (i--) {
                    if (callbacks[i] === callback) callbacks.splice(i, 1);
                }
            };

            return processFunc;
        })(),

        isEmpty: function (v) {
            if (v === undefined || v === null) return true;
            if (typeof v === 'string') {
                v = v.trim();
                if (v === '') return true;
            } else if (Array.isArray(v)) {
                if (v.length === 0) return true;
            }
            return false;
        },

        templates: (function () { // Parst alle handlebars-Templates in der Seite
            Handlebars.registerHelper({

                // Check Functions
                isFilled: function (v) {
                    return !util.isEmpty(v);
                },
                isEmpty: function (v) {
                    return util.isEmpty(v);
                },
                oneset: function () {
                    var args = Array.prototype.slice.call(arguments);
                    for (var i = 0; i < args.length; i++) {
                        if (args[i] && args[i] != undefined) return true;
                    }
                    return false;
                },
                allset: function () {
                    var args = Array.prototype.slice.call(arguments);
                    for (var i = 0; i < args.length; i++) {
                        if (!args[i]) return false;
                    }
                    return true;
                },

                // Boolean
                not: function (v1) {
                    return !v1;
                },
                and: function () {
                    return Array.prototype.slice.call(arguments).every(Boolean);
                },
                or: function () {
                    return Array.prototype.slice.call(arguments, 0, -1).some(Boolean);
                },

                // Compare
                of: function () {
                    var args = Array.prototype.slice.call(arguments),
                        compareValue = args.shift();

                    return args.indexOf(compareValue) != -1;
                },
                eq: function (v1, v2) {
                    return v1 === v2;
                },
                ne: function (v1, v2) {
                    return v1 !== v2;
                },
                lt: function (v1, v2) {
                    return v1 < v2;
                },
                gt: function (v1, v2) {
                    return v1 > v2;
                },
                lte: function (v1, v2) {
                    return v1 <= v2;
                },
                gte: function (v1, v2) {
                    return v1 >= v2;
                },

                // String
                unitFileSize: function (bytes, decimals) {
                    return util.unitFileSize(bytes, decimals);
                },
                local: function (v) {
                    return util.localNumber(v);
                },
                localDate: function (date) {
                    return date.toLocaleDateString(MJK.locale);
                },

                // Math
                add: function () {
                    var v = 0;
                    for (var i = 0; i < arguments.length; i++) v += arguments[i];
                    return v;
                },
                subtract: function (v1, v2) {
                    return v1 - v2
                },
                multiply: function (v1, v2) {
                    var v = 1;
                    for (var i = 0; i < arguments.length; i++) v *= arguments[i];
                    return v;
                },
                divide: function (v1, v2) {
                    return v2 ? v1 / v2 : undefined;
                }
            });

            var instance, tempsArray = instance = [], tempsById = {}, tempsByGroup = {}, tempsByTag = {};
            // MJK.log('Processing ', $('[data-handlebars-id]').length, ' templates');

            instance.init = function (templateSrc, id) {

                if (!templateSrc || typeof templateSrc !== 'string') {
                    // MJK.log('Could not generate template from ', arguments);
                    return;
                }

                //Fix Partial syntax from {{+ to {{> because it is otherwise blocked by patternlab
                templateSrc = templateSrc.replace(/\{\{\+/g, '{{>');

                var Template = Handlebars.compile(templateSrc);
                Template.id = id; // Id des Templates (String)
                Template.src = templateSrc; // Quelltext des Templates (String)
                Template.instances = [];
                Template.$lastInstance = undefined;

                if (Template.id) {
                    tempsArray.push(Template);
                    tempsById[Template.id] = Template;
                }

                Template.create = function (value) {
                    value = value || {};
                    value._texts = util.texts;
                    value._objects = util.objects;

                    var html = Template(value);
                    //html = html.replace(/\&amp\;shy\;/g, '&shy;');
                    var $inst = Template.$lastInstance = $(html);
                    MJK.util.processDynamic($inst);

                    // MJK.log('Template.create(', Template, value, ') => ', $inst[0],html);
                    return $inst;
                };

                Template.append = function (value) {
                    var $inst = Template.create(value);
                    if (Template.$parent.length) Template.$parent.append($inst);
                    return $inst;
                };

                Template.prepend = function (value) {
                    var $inst = Template.create(value);
                    if (Template.$parent.length) Template.$parent.prepend($inst);
                    return $inst;
                };

                Template.replace = function (value) {
                    if (Template.$lastInstance) Template.$lastInstance.remove();

                    var $inst = Template.create(value);
                    if (Template.$parent.length) Template.$parent.prepend($inst);
                    // MJK.log('Template.replace(', Template, value, ') => ', $inst[0]);
                    return $inst;
                };

                return Template;
            };

            $('[data-handlebars-id]').each(function () {
                var $templateSrc = $(this),
                    templateSrc = $templateSrc.html(),
                    id = ($templateSrc.attr('data-handlebars-id') || '').trim();

                // MJK.log('Handlebar template [id:', id, '] = ', templateSrc);
                var Template = instance.init(templateSrc, id)

                Template.group = Template.id.split('.')[0]; // Gruppe des Templates (String)
                Template.tags = ($templateSrc.attr('data-handlebars-tags') || '').split(','); // Tags des Templates (Array)
                Template.$src = $templateSrc; // Ursprungselement des (jQuery)
                Template.$parent = $templateSrc.parent(); // Elternelement des Templates im HTML (jQuery)

                if (Template.group) {
                    tempsByGroup[Template.group] = tempsByGroup[Template.group] || [];
                    tempsByGroup[Template.group].push(Template);
                }

                Template.tags.forEach(function (tag, i) {
                    tag = tag.trim();
                    Template.tags[i] = tag;

                    tempsByTag[tag] = tempsByTag[tag] || [];
                    tempsByTag[tag].push(Template);
                });

                $templateSrc.remove();
            });

            instance.byId = tempsById;
            instance.byTag = tempsByTag;
            instance.byGroup = tempsByGroup;
            instance.get = function (identifier) {
                if (typeof identifier === 'string') {
                    identifier = {
                        id: identifier
                    };
                }

                if (identifier.id) {
                    return instance.byId[identifier.id]
                }

                if (identifier.group) {
                    var instances = instance.byGroup[identifier.group];
                    if (instances && identifier.tag) {
                        var temp;
                        instances.forEach(function (inst, i) {
                            if (inst.tags.indexOf(identifier.tag) !== -1) {
                                temp = inst;
                            }
                        });
                        if (temp) return temp;
                    } else {
                        return instances;
                    }
                }

                if (identifier.tag) {
                    return instance.byTag[identifier.tag];
                }

            };
            //// MJK.log('MJK.util.templates:', instance);

            return instance;
        })(),

        texts: (function () {
            var texts = {};
            $('[data-text-id]').each(function () {
                var $json = $(this),
                    id = $json.attr('data-text-id'),
                    json = $json.html().trim();

                try {
                    var value = JSON.parse(json);

                    if (texts.hasOwnProperty(id)) {
                        MJK.log('Duplicate text id ', id);
                        return;
                    }

                    $json.remove();
                    MJK.log('texts.', id, ' = ', value);

                    texts[id] = value;
                } catch (e) {
                    MJK.log('MJK.util.texts Error reading ',id,' => ',$json[0]);
                }
            });
            texts.get = function (jsonId, id, defaultValue) {
                var json = texts[jsonId],
                    value = json ? (id ? json[id] : json) : undefined;
                //MJK.log('MJK.util.texts.get(', jsonId, ', ', id, ') = ', value);
                return value || defaultValue;
            };

            MJK.log('MJK.util.texts:', texts);
            return texts;
        })(),

        objects: (function () {
            var objects = {};
            $('[data-object-id]').each(function () {
                var $json = $(this),
                    id = $json.attr('data-object-id'),
                    json = $json.html().trim();

                try {
                    var value = JSON.parse(json);

                    if (objects.hasOwnProperty(id)) {
                        MJK.log('Duplicate object id ', id);
                        return;
                    }

                    $json.remove();
                    MJK.log('objects.', id, ' = ', value);

                    objects[id] = value;
                } catch (e) {
                    MJK.log('MJK.util.objects Error reading ',id,' => ',$json[0]);
                }
            });
            objects.get = function (jsonId, defaultValue) {
                return objects[jsonId] || defaultValue;
            };

            MJK.log('MJK.util.objects:', objects);
            return objects;
        })(),

        prefixIf: function (value, prefix) {
            return value ? prefix + value : value;
        },

        suffixIf: function (value, suffix) {
            return value ? value + suffix : value;
        },

        surroundIf: function (value, prefix, suffix) {
            suffix = suffix || '';
            prefix = prefix || '';
            return value ? prefix + value + suffix : value;
        },

        round: function (num, digits) {
            if (!digits) return Math.round(num);
            var f = Math.pow(10, digits || 0);
            return Math.round(num * f) / f;
        },

        floor: function (num, digits) {
            if (!digits) return Math.floor(num);
            var f = Math.pow(10, digits || 0);
            return Math.floor(num * f) / f;
        },

        localNumber: function (v) {
            return Number(v).toLocaleString();
        },

        copyToOb: function (aimOb, data) {
            if (data) { // TODO: Ggf Unterstützung für kommaseparierte Keys hinzufügen.
                for (var key in data) if (data.hasOwnProperty(key)) {
                    aimOb.key = data[key];
                }
            }
            return aimOb;
        },

        unitFileSize: (function () {
            var base = 1024,
                units = ['Byte', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

            return function (value, digits) {
                value = parseInt(value);
                digits = parseInt(digits);

                if (isNaN(value)) return value;
                if (value === 0) return '0 Bytes';

                if (!digits || isNaN(digits)) digits = 2;
                var exp = Math.floor(Math.log(value) / Math.log(base)),
                    str = util.suffixIf((util.round(value / Math.pow(base, exp), digits)).toLocaleString(), '&nbsp;' + units[exp]);

                return str;
            };
        })(),

        smoothScroll: function (target) {
            $('html, body').stop().animate({
                scrollTop: $(target).offset().top - 100
            }, 600);
            return false;
        },

        //Checks if css object-fit is available and otherwise uses a polyfill with background-image
        checkObjectFit: function () {
            if (!Modernizr.objectfit) {
                $('.product-stage-img').each(function () {
                    var $container = $(this),
                        imgUrl = $container.find('img').prop('src');
                    if (imgUrl) {
                        $container
                            .css('backgroundImage', 'url(' + imgUrl + ')')
                            .addClass('object-fit-contain');
                    }
                });
            }
        },

        uniqueID: function () {
            return Math.round(new Date().getTime() + (Math.random() * 100));
        },

        findHighestZIndex: function () {
            var elems = document.getElementsByTagName("*");
            var highest = 0;
            for (var i = 0; i < elems.length; i++) {
                var zindex = document.defaultView.getComputedStyle(elems[i], null).getPropertyValue("z-index");
                if ((zindex > highest) && (zindex != 'auto')) {
                    highest = zindex;
                }
            }
            return highest;
        },

        getUrlParameterByName: function (name, url) {
            url = url || window.location.href;
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        },

        convertToEventDispatcher: function (instance) {
            var eventTarget = document.createTextNode(null);
            instance.addEventListener = eventTarget.addEventListener.bind(eventTarget);
            instance.removeEventListener = eventTarget.removeEventListener.bind(eventTarget);
            instance.dispatchEvent = eventTarget.dispatchEvent.bind(eventTarget);
            instance.triggerEvent = function (eventName) {
                MJK.log('triggerEvent', eventName);
                instance.dispatchEvent(new CustomEvent(eventName));
            };
            return instance;
        }
    }
})(jQuery);
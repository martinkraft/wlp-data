$(function () {
    MJK.log('start Module');

    Handlebars.registerHelper({
        qidFromUri: function (v) {
            //MJK.log('qidFromUri(',v, arguments,')');
            return v.split('/').pop();
        },
        searchPerson: function (v) {
            //MJK.log('search(',v, arguments,')');
            if (!searchData.currentQ) return true;

            for (var i = 0; i < arguments.length; i++) {
                if (String(arguments[i]).toLowerCase().indexOf(searchData.currentQ) !== -1) return true;
            }
            return false;
        }
    });

    var sparql = (function () {
        var $ob = $('[data-sparql-id]'),
            str = $ob.text();

        MJK.log('sparql:', $ob[0]);

        return str;
    })();

    var searchData = {},
        personData = {};

    MJK.log(MJK.util.templates);

    $.getJSON('https://query.wikidata.org/sparql?format=json&query=' + encodeURIComponent(sparql), {}, function (data) {

        var results = data.results.bindings;
        searchData.persons = data.results.bindings;
        searchData.personsByWikiData = {};
        searchData.persons.forEach(function (item) {
            if(item.parliamentaryGroupLabel){
                item.parliamentaryGroupLabel.value = item.parliamentaryGroupLabel.value.replace('Parteiloser', 'fraktionslos');
                item.parliamentaryGroupLabel.shortValue = item.parliamentaryGroupLabel.value.replace('-Bundestagsfraktion', '').replace('Bundestagsfraktion', '').replace('Fraktion', '').replace('Parteiloser', 'fraktionslos').trim();
            }

            item.person.value = item.person.value.replace('http://', 'https://');
            item.wikidata = item.person.value.split('/').pop();
            searchData.personsByWikiData[item.wikidata] = item;
        });

        updateSearch();
        if (window.location.hash == '') window.location.href = '#Search';
    });

    $('.logo').click(function (e) {
        try {
            if (!document.fullscreenElement) {
                $('body')[0].requestFullscreen();
                $('body')[0].webkitRequestFullscreen();
            } else {
                document.exitFullscreen();
                document.webkitExitFullscreen();
            }
        } catch (error) {
            MJK.log('Error attempting to enable full-screen mode:', error);
        }
    });
    //$('input[name="q"]').change(updateSearch);
    $('input[name="q"]').on('input', updateSearch);
    $('form[data-action="search"]').submit(updateSearch);
    MJK.$doc.on('click', 'a[target="webview"]', function (e) {
        window.location.href = '#WebView';
    });
    $(window).on('hashchange', function (e) {
        MJK.log('hash:', window.location.hash, ' / ', e);
        if (window.location.hash == '#Search') {
            MJK.log('focus')
            $('#Search input[type="text"]').focus();
        }
    });

    MJK.$doc.on('click', 'a[data-show-person]', function (e) {
        var person = searchData.personsByWikiData[$(this).attr('data-show-person')];

        if (person) {
            e.preventDefault();
            updatePerson(person);
            window.location.href = '#NameTag';
        }
    });

    function updatePerson(person) {
        personData.person = person;

        MJK.log('updatePerson ', personData);
        MJK.util.templates.byTag['person'].forEach(function (SearchTemplate) {
            SearchTemplate.replace(personData);
        });
    }

    function updateSearch(e) {
        if (e && e.type == 'submit') e.preventDefault();

        searchData.currentQ = $('.view-search input[name="q"]').val();
        if (searchData.currentQ) {
            searchData.currentQ = searchData.currentQ.toLowerCase().trim();
            if (searchData.currentQ == '') searchData.currentQ = undefined;
        }
        MJK.log('updateSearch ', searchData);
        MJK.util.templates.byTag['searchResults'].forEach(function (SearchTemplate) {
            SearchTemplate.replace(searchData);
        });
    }
});